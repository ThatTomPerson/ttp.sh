---
title: Frost
subtitle: Package Installer
date: "2018-01-25"
type: project
layout: project
tags: ["project", "aws", "ecs"]
repo: https://github.com/thattomperson/frost
---

## Package Installer?
We don't manage anything, we just download packages and render out some templates for Composer and NPM 

{{< install frost >}}
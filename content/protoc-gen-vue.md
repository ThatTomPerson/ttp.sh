---
title: Proto Vue
subtitle: docker compose on steroids
date: "2018-01-25"
type: project
layout: project
tags: ["project", "aws", "ecs"]
repo: gitlab.com/dps-pub/open-source/protoc-gen-vue
---

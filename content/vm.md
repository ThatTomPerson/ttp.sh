---
title: VM
subtitle: docker compose on steroids
date: "2018-01-25"
type: project
layout: project
tags: ["project", "docker", "docker-compose"]
repo: https://github.com/thattomperson/vm
---

{{< install vm >}}